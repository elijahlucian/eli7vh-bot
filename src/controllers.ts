import { get } from './api'
import { ContractDetails } from './types'
import { addContract } from './utils'

export class Stats {
  static base = (action: string, address: string) => `?module=stats&action=${action}&contractaddress=${address}`

  static async totalSupply(address: string) {
    return get<string>(this.base('tokensupply', address))
  }
  static async circulatingSupply(address: string) {
    return get<string>(this.base('tokenCsupply', address))
  }
}

export class Contract {
  static base = (action: string, address: string) => `?module=contract&action=${action}&address=${address}`

  static async details(address: string) {
    return get<ContractDetails[]>(this.base('getsourcecode', address))
  }
}

export class Account {
  static base = (action: string, address: string) => `?module=account&action=${action}&address=${address}`

  static async balance(address: string) {
    get<string>(this.base('balance', address))
  }

  static async tokenBalance(address: string) {
    get(this.base('tokenbalance', address))
  }
}

export const getLatestPancake = async () => {
  // added liquidity to pancake swap
}

export const getMcap = async () => {
  // added liquidity to pancake swap
}
