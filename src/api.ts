import axios from 'axios'
import { BSCRes } from './types'

const API_URL = 'https://api.bscscan.com/api'

axios.defaults.baseURL = API_URL
axios.interceptors.request.use((config) => {
  config.url += `&apikey=${process.env.BSCSCAN_API_KEY}`
  return config
})

export const get = async <T>(url: string) => {
  const { data } = await axios.get<BSCRes<T>>(url)

  if (data.status === '0') return null

  return data.result
}
