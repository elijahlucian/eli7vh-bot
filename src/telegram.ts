import TelegramBot from 'node-telegram-bot-api'
import { Contract } from './controllers'

export const makeBot = (token?: string) => {
  if (!token) {
    console.error('no api key')
    return
  }

  return new TelegramBot(token, { polling: true })
}

export const botRouter = (bot?: TelegramBot) => {
  if (!bot) {
    console.log('no bot available')
    return
  }

  console.log('configuring bot')

  bot.on('photo', async (message) => {
    console.log('got an image!')
  })
  bot.on('document', async (message) => {
    console.log('got an "document"!')
  })

  bot.on('channel_post', (message) => {
    console.log('channel post')
  })

  bot.on('message', async (message) => {
    if (message.reply_to_message) {
      console.log('Replied to', message.reply_to_message.message_id)
      console.log('>>', message.chat.username || message.chat.title, message.text)
    } else {
      console.log(message.chat.username || message.chat.title, message.text)
    }

    if (!message.text?.match(/^!/)) return

    if (message.text.match(/^!exclude /)) {
      // add text to after exclusions
      const match = message.text.replace(/^!exclude /, '')
      bot.sendMessage(message.chat.id, `adding to list of exclusions: ${match}`)
    }

    if (message.text.match(/^!list/)) {
      // list all exclusions with numbers for lookup
    }

    if (message.text.match(/^!commands/)) {
      const response = `
!contract [contract_address] => shows contract for address
!list => shows list of exclusions
!exclude [match] => adds exclusion to logic
      `
      bot.sendMessage(message.chat.id, response)
    }

    if (message.text.match(/^!contract [\w]+$/)) {
      const kw = message.text.split(' ')

      const contractId = kw[1]

      console.log('fetching contract', contractId)
      const res = await Contract.details(contractId)

      if (!res) {
        console.log('Contract not found!')
        bot.sendMessage(message.chat.id, 'Contract not found!')
        return
      }

      const contract = res[0]
      const response = contract.SourceCode.split('\n').slice(0, 20).join('\n')

      if (!response) {
        console.log(contract.ContractName)
      }

      bot.sendMessage(message.chat.id, response)
    }
  })
}
