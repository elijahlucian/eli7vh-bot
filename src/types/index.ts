export type BSCRes<T> = {
  status: "0" | "1";
  message: "OK" | "NOTOK";
  result: T;
};

export type ContractDetails = {
  SourceCode: string; //
  ABI: string;
  ContractName: string; // 'Shibucoin',
  CompilerVersion: string; // 'v0.5.16+commit.9c3226ce',
  OptimizationUsed: string; // '0',
  Runs: string; // '200',
  ConstructorArguments: string; // '',
  EVMVersion: string; // 'Default',
  Library: string; // '',
  LicenseType: string; // 'MIT',
  Proxy: string; // '0',
  Implementation: string; // '',
  SwarmSource: string; // 'bzzr://e803f5b8f69a167a0eb7e5793b7caf1da1afed244d2022f655b1277e4d2c7df4'
};
