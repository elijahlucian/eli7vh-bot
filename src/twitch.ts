import tmi from 'tmi.js'

export const makeTwitchBot = (token?: string) => {
  if (!token) {
    console.log('twitch aauth token not supplied')
  }

  return tmi.Client({
    // options: { debug: true },
    connection: {
      reconnect: true,
    },
    channels: ['eli7vh'],
    identity: {
      username: 'dankbot',
      password: token,
    },
  })
}
